# ---------------------------------------------------------------------------- #

#        ______   _____  ________      ________ 
#      |  ____| / ____| |__   __| /\  |__   __|
#     | |__    | (___     | |   /  \    | |   
#    |  __|    \___ \    | |  / /\ \   | |   
#   | |____   ____) |   | |  /____ \  | |   
#  |______   |_____/   |_| /_/    \_\|_|   
#  
#         Consultoria estatística 
#

# ---------------------------------------------------------------------------- #

# 0.1 - Dos Autores ----
#
# Atividade gestão - Grupo 3 - ESTAT
# Bruno Gondim Toledo - Trainee ESTAT | E-mail: bruno.gondim@aluno.unb.br
# Jéssica Gois - Trainee ESTAT | E-mail:
# Ana Luisa - Trainee ESTAT | E-mail:
# Felipe Adriano - Trainee ESTAT | E-mail:
# Brasília, novembro de 2022
# Gitlab do projeto: https://gitlab.com/penasta/atividade-de-gestao-grupo-3

# ---------------------------------------------------------------------------- #

# 0.2 - Do software ----
#
# linguagem de programação utilizada: R (versão 4.2.1).
# IDE utilizada: RStudio Desktop 2022.07.1+554
# Pacotes R utilizados (em ordem de utilização no código): 
# pacman,doParallel,tidyverse
#
if (!require("pacman")) install.packages("pacman")
# p_load(installr)
# updateR()
# Iremos utilizar o pacote pacman para instalar e carregar os pacotes no
# decorrer do código.
# Optaremos por fazer o carregamento dos pacotes no decorrer do código, conforme
# a necessidade. Com isso, visamos deixar mais claro a partir de qual etapa é
# necessário cada um dos pacotes utilizados.

# ---------------------------------------------------------------------------- #

# 0.3: Do hardware ----
#
# Nosso default nos comandos que explicitarei o número de thread à serem 
# utilizadas será 80% das threads disponíveis, arredondado. Caso queira 
# utilizar um número diferente de threads, basta alterar o comando abaixo
# conforme conveniência.
#
p_load(doParallel)
threads=round(as.numeric(detectCores()*0.8))
#
# Especificações das máquina utilizadas para fazer e rodar o código:
#
# Bruno:
# Lenovo Ideapad 3
# Windows 11
# CPU AMD Ryzen 7 5700u (16 threads)
# RAM 20(16 + 4)GB DDR4 3200 Mhz
# GPU AMD Radeon Vega 8 (Integrada)
# 256 GB SSD M.2 2242 PCIe NVMe
#

# ---------------------------------------------------------------------------- #

# 0.4: O projeto ----
#
# Dashboard Financeiro

# ANÁLISES:
# 01)Análise temporal de entradas e saídas
# 02)Principal gasto (categoria)
# 03)Clientes inadimplentes
# 04)Notas fiscais (sim e não)
# 05)Média de parcelamentos
# 06)Ticket médio
# 07)Gasto que cada membro e empresa tem
# 08)Comparação entre PF e PJ

# REQUISITOS
# Colocar um filtro que separa os dados por mês
# Logo da empresa
# Cores da empresa
# Indicadores
# Dashboard feito no Google DataStudio

# ---------------------------------------------------------------------------- #

# 1: Iniciando o projeto ----

# ---------------------------------------------------------------------------- #

# 1.0.1: Definindo as padronizações da empresa

cores_estat <- c('#A11D21','#663333','#FF6600','#CC9900','#CC9966','#999966','#006606','#008091','#003366','#041835','#666666')

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

# ---------------------------------------------------------------------------- #

# 1.1: Carregando os bancos ----
p_load(tidyverse)
projetos <- readxl::read_excel("bancos/dados_financeiros_accenture.xlsx", sheet = "Projetos")
fluxocaixa <- readxl::read_excel("bancos/dados_financeiros_accenture.xlsx", sheet = "Fluxo de caixa")

# 1.1.1: Pequenos ajustes no banco ----
# 1.1.1.1: DF projetos ----

projetos <- projetos %>%
  drop_na()
projetos$Projetos <- factor(projetos$Projetos)
projetos$`Mês de início` <- factor(projetos$`Mês de início`)


# 1.1.1.2: DF fluxocaixa ----
p_load(lubridate)

fluxocaixa$Data <- as.Date(fluxocaixa$Data)
fluxocaixa$Dia <- factor(fluxocaixa$Dia)
fluxocaixa$Mês <- factor(fluxocaixa$Mês)
fluxocaixa$Ano <- factor(fluxocaixa$Ano)
fluxocaixa$Origem <- factor(fluxocaixa$Origem)
fluxocaixa$`Quem?` <- factor(fluxocaixa$`Quem?`)
fluxocaixa$Categoria <- factor(fluxocaixa$Categoria)
fluxocaixa$`Nota Fiscal` <- factor(fluxocaixa$`Nota Fiscal`)
fluxocaixa <- fluxocaixa %>%
  mutate(`Nota Fiscal` = case_when(`Nota Fiscal` == 'Sim' ~ 'Sim',
                                   `Nota Fiscal` == 'sim' ~ 'Sim',
                                   `Nota Fiscal` == 'Não' ~ 'Não'))
fluxocaixa$`Nota Fiscal` <- factor(fluxocaixa$`Nota Fiscal`)

# 1.2: Juntando as planilhas ----

df <- full_join(
  x=fluxocaixa,
  y=projetos,
  by = c("Quem?" = "Projetos"))

# ---------------------------------------------------------------------------- #

# 2: ANÁLISES ----

# ---------------------------------------------------------------------------- #

# 2.1: Análise temporal de entradas e saídas (Bruno) ----

analisetemporal <- fluxocaixa %>%
  select(Data,Valor) %>%
  group_by(Data)

ggplot(analisetemporal) +
  aes(x=Data, y=Valor, group=1) +
  geom_line(size=1,colour="#A11D21") + geom_point(colour="#A11D21",size=2) +
  labs(title="Fluxo de caixa da Accenture no ano de 2022" ,x="Mês", y="Fluxo de caixa em Reais") +
  theme_estat() # Gráfico do fluxo de caixa

# ---------------------------------------------------------------------------- #

# 2.2: Principal gasto (categoria) ----

# ---------------------------------------------------------------------------- #

# 2.3: Clientes inadimplentes (Bruno) ----

inadimplentes <- projetos %>%
  select(Projetos,`Valor a Receber`) %>%
  filter(`Valor a Receber` > 0)

inadimplentes # Tabela dos clientes inadimplentes

# ---------------------------------------------------------------------------- #

# 2.4: Notas fiscais (sim e não) ----

# ---------------------------------------------------------------------------- #

# 2.5: Média de parcelamentos ----

# ---------------------------------------------------------------------------- #

# 2.6: Ticket médio ----

# ---------------------------------------------------------------------------- #

# 2.7: Gasto que cada membro e empresa tem ----

# ---------------------------------------------------------------------------- #

# 2.8: Comparação entre PF e PJ ----

# ---------------------------------------------------------------------------- #

# 3: Exportando as tabelas arrumadas ----
# (Vou deixar os códigos em comentário pois só precisa exportar 1x)

# 3.0: Tabela junta ----
# write.csv(df,"bancos/accenture.csv", row.names = FALSE, na="")

# 3.1: Tabela análise 1 ----
# write.csv(analisetemporal,"bancos/analisetemporal.csv", row.names = FALSE, na="")

# 3.3: Tabela análise 3 ----
# write.csv(inadimplentes,"bancos/inadimplentes.csv", row.names = FALSE, na="")

# ---------------------------------------------------------------------------- #