# Atividade de gestão - Grupo 3 - ESTAT

## Nome
Projeto Atividade de gestão - Grupo 3
Integrantes do grupo:
- Bruno Gondim Toledo
- Ana Luisa
- Jéssica Gois
- Felipe Adriano

Nome provisório do relatório: não há.

## Descrição
Repositório público de arquivos utilizados para a realização da Atividade de gestão - Grupo 3  processo seletivo da ESTAT - Empresa júnior de estatística, de autoria de: Bruno Gondim Toledo, Ana Luisa, Jéssica Gois e Felipe Adriano.

## Software
O sistema operacional utilizado na elaboração de todos os arquivos deste repositório foi o Windows 11.
Para a utilização plena dos arquivos aqui contidos, é necessário ter ao menos instalado em sua máquina:
R (versão 4.2.1), Rstudio, algum navegador (para utilização do Google datastudio), além de aplicativos padrão da Microsoft para visualização de arquivos .txt, .png, etc, ou outros equivalentes.
Não foi testado em outros sistemas operacionais. Não há como garantir o funcionamento completo ou parcial do projeto em outro SO que não o Windows 11. Não há como garantir o funcionamento completo ou parcial dos códigos R em outra IDE que não o Rstudio.

## Aplicação
A definir.

## Suporte
E-mail dos autores: 
- Bruno: bruno.gondim@aluno.unb.br / Alternativo: brunogtoledo96@gmail.com
- Ana Luisa:
- Jéssica Gois: 
- Felipe Adriano: 

Telefone/WhatsApp dos autores:

- Bruno: +55 (61) 9 9544-6161
- Ana Luisa: 
- Jéssica Gois: 
- Felipe Adriano: 

Os autores não fornecem garantia ou suporte acerca dos arquivos neste repositório.

## Contribuidores
A elaboração das análises estatísticas e dos relatórios presente neste repositório é de inteira responsabilidade dos autores. Entretando, foram utilizados/as padronizações, guias e direcionamentos fornecidos/as pela empresa ESTAT - Consultoria Estatística. A utilização dos materiais desta empresa contidas neste projeto seguem as políticas de privacidade da empresa.

## Licença
Propriedade particular-pública. A utilização, publicação e cópia dos arquivos contidos neste repositório manufaturadas pelos autores, está à definir.
A utilização dos materiais da empresa ESTAT - Consultoria Estatística contidas neste projeto (padronizações, guias, direcionamentos, etc) seguem as políticas de privacidade da empresa.

## Etapas do projeto
A definir.

## Status do projeto
 Não iniciado.
